# DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](DefaultApi.md#create) | **POST** /api/rest/project/create | 
[**create_0**](DefaultApi.md#create_0) | **POST** /api/rest/task/create | 
[**findAll**](DefaultApi.md#findAll) | **GET** /api/rest/project/findAll | 
[**findAll_0**](DefaultApi.md#findAll_0) | **GET** /api/rest/task/findAll | 
[**findById**](DefaultApi.md#findById) | **GET** /api/rest/project/findById/{id} | 
[**findById_0**](DefaultApi.md#findById_0) | **GET** /api/rest/task/findById/{id} | 
[**login**](DefaultApi.md#login) | **GET** /api/rest/authentication/login | 
[**logout**](DefaultApi.md#logout) | **GET** /api/rest/authentication/logout | 
[**profile**](DefaultApi.md#profile) | **GET** /api/rest/authentication/profile | 
[**removeOneById**](DefaultApi.md#removeOneById) | **DELETE** /api/rest/project/removeById/{id} | 
[**removeOneById_0**](DefaultApi.md#removeOneById_0) | **DELETE** /api/rest/task/removeById/{id} | 
[**updateProjectById**](DefaultApi.md#updateProjectById) | **PUT** /api/rest/project/updateById | 
[**updateTaskById**](DefaultApi.md#updateTaskById) | **PUT** /api/rest/task/updateById | 


<a name="create"></a>
# **create**
> ProjectDTO create(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.create(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#create");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  | [optional]

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="create_0"></a>
# **create_0**
> TaskDTO create_0(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.create_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#create_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  | [optional]

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findAll"></a>
# **findAll**
> List&lt;ProjectDTO&gt; findAll()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<ProjectDTO> result = apiInstance.findAll();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findAll");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ProjectDTO&gt;**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findAll_0"></a>
# **findAll_0**
> List&lt;TaskDTO&gt; findAll_0()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<TaskDTO> result = apiInstance.findAll_0();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findAll_0");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TaskDTO&gt;**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findById"></a>
# **findById**
> ProjectDTO findById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    ProjectDTO result = apiInstance.findById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findById_0"></a>
# **findById_0**
> TaskDTO findById_0(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    TaskDTO result = apiInstance.findById_0(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findById_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="login"></a>
# **login**
> Boolean login(username, password)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String username = "username_example"; // String | 
String password = "password_example"; // String | 
try {
    Boolean result = apiInstance.login(username, password);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#login");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  |
 **password** | **String**|  |

### Return type

**Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="logout"></a>
# **logout**
> logout()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    apiInstance.logout();
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#logout");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="profile"></a>
# **profile**
> UserDTO profile()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    UserDTO result = apiInstance.profile();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#profile");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserDTO**](UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="removeOneById"></a>
# **removeOneById**
> removeOneById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    apiInstance.removeOneById(id);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#removeOneById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeOneById_0"></a>
# **removeOneById_0**
> removeOneById_0(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    apiInstance.removeOneById_0(id);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#removeOneById_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateProjectById"></a>
# **updateProjectById**
> ProjectDTO updateProjectById(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.updateProjectById(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateProjectById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  | [optional]

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateTaskById"></a>
# **updateTaskById**
> TaskDTO updateTaskById(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.updateTaskById(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateTaskById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  | [optional]

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

