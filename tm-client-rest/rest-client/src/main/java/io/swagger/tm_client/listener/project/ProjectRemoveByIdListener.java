package io.swagger.tm_client.listener.project;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import io.swagger.tm_client.event.ConsoleEvent;
import io.swagger.tm_client.listener.AbstractListener;
import io.swagger.tm_client.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractListener {

    @NotNull
    @Autowired
    private DefaultApi api;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @EventListener(condition = "@projectRemoveByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) throws ApiException {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        api.removeOneById(id);
        System.out.println("[OK]");
    }

}
