package io.swagger.tm_client;

import io.swagger.tm_client.bootstrap.Bootstrap;
import io.swagger.tm_client.config.ClientConfiguration;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Client {

    public static void main(String... args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        context.registerShutdownHook();
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}

