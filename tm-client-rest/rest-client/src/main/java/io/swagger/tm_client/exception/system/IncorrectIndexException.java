package io.swagger.tm_client.exception.system;

import org.jetbrains.annotations.NotNull;
import io.swagger.tm_client.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    @NotNull
    public IncorrectIndexException(@NotNull final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }

}