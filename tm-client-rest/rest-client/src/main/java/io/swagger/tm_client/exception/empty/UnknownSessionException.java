package io.swagger.tm_client.exception.empty;

import io.swagger.tm_client.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UnknownSessionException extends AbstractException {

    @NotNull
    public UnknownSessionException() {
        super("Error! Session is unknown...");
    }

}