package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class SessionService {

    public static final String HEADER_COOKIE = "Cookie";

    public static final String HEADER_SET_COOKIE = "Set-Cookie";

    public static void setMaintain(@Nullable final Object port) {
        @NotNull final BindingProvider bindingProvider = (BindingProvider) port;
        @NotNull final Map<String, Object> map = bindingProvider.getRequestContext();
        map.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static List<String> getListCookieRow(@Nullable final Object port) {
        @Nullable final Map<String, Object> httpResponseHeaders = getHttpResponseHeaders(port);
        if (httpResponseHeaders == null) return null;
        return (List<String>) httpResponseHeaders.get(HEADER_SET_COOKIE);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getHttpResponseHeaders(@Nullable final Object port) {
        @Nullable final Map<String, Object> responseContext = getResponseContext(port);
        if (responseContext == null) return null;
        return (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
    }

    @Nullable
    public static Map<String, Object> getResponseContext(@Nullable final Object port) {
        @Nullable final BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getResponseContext();
    }

    @Nullable
    public static BindingProvider getBindingProvider(@Nullable final Object port) {
        if (port == null) return null;
        return (BindingProvider) port;
    }

    public static void setListCookieRowRequest(@Nullable final Object port, @Nullable final List<String> value) {
        if (getRequestContext(port) != null && getHttpRequestHeaders(port) == null) {
            getRequestContext(port).put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<>());
        }
        @Nullable final Map<String, Object> httpRequestHeaders = getHttpRequestHeaders(port);
        if (httpRequestHeaders == null) return;
        httpRequestHeaders.put(HEADER_COOKIE, value);
    }

    @Nullable
    public static Map<String, Object> getRequestContext(@Nullable final Object port) {
        @Nullable final BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getRequestContext();
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getHttpRequestHeaders(@Nullable final Object port) {
        @Nullable final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext == null) return null;
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

}