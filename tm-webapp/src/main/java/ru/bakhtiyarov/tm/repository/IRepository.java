package ru.bakhtiyarov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

@Repository
public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {
}