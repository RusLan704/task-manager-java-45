package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.service.IService;
import ru.bakhtiyarov.tm.entity.AbstractEntity;
import ru.bakhtiyarov.tm.repository.IRepository;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity, R extends IRepository<E>> implements IService<E> {

    @NotNull
    protected abstract R getRepository();

    @Override
    public void addAll(@Nullable List<E> records) {
        if (records == null) return;
        @NotNull IRepository<E> repository = getRepository();
        repository.saveAll(records);
    }

    @Override
    @Nullable
    public E save(@Nullable final E record) {
        if (record == null) return null;
        @NotNull IRepository<E> repository = getRepository();
        return repository.save(record);
    }

}