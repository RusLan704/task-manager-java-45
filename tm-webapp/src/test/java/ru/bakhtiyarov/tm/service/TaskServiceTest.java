package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.Application;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.RoleType;
import ru.bakhtiyarov.tm.repository.IProjectRepository;
import ru.bakhtiyarov.tm.repository.ITaskRepository;
import ru.bakhtiyarov.tm.repository.IUserRepository;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class TaskServiceTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IUserService userService;

    @NotNull
    private Task task;

    private User user;

    @NotNull
    private Project project;

    @Before
    @Transactional
    public void initData() {
        user = userService.create("login", "pass", RoleType.ADMIN);
        project = new Project("name", "description");
        project.setUser(user);
        projectRepository.save(project);
        task = new Task("name", "description");
        task.setUser(user);
        task.setProject(project);
        taskService.save(task);

        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("login", "pass");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    @Transactional
    public void deleteData() {
        taskRepository.deleteAll();
        userRepository.deleteAll();
        projectRepository.deleteAll();
    }

    @Test
    public void testCreate() {
        Assert.assertEquals(1, taskService.findAll(user.getId()).size());
        @Nullable final Task task = new Task("new name", "new description");
        task.setUser(user);
        task.setProject(project);
        taskService.create(user.getId(),task, project.getId());
        Assert.assertEquals(2, taskService.findAll(user.getId()).size());
        @Nullable final Task taskAdded = taskService.findById(task.getId());
        Assert.assertEquals(task.getId(), taskAdded.getId());
        Assert.assertEquals(task.getName(), "new name");
        Assert.assertEquals(task.getDescription(), "new description");
    }

    @Test
    public void testUpdateTaskById() {
        Assert.assertEquals(1, taskService.findAll(user.getId()).size());
        @Nullable final Task task1 = new Task("new name", "new description");
        task1.setId(task.getId());
        task1.setUser(user);
        task1.setProject(project);
        taskService.updateTaskById(user.getId(), task1);
        @Nullable final Task taskUpdated = taskService.findById(task.getId());
        Assert.assertEquals(taskUpdated.getId(), task.getId());
        Assert.assertEquals(taskUpdated.getName(), "new name");
        Assert.assertEquals(taskUpdated.getDescription(), "new description");
    }

    @Test
    public void testFindAllByUserId() {
        Assert.assertEquals(1, taskService.findAll(user.getId()).size());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(1, taskService.findAll().size());
    }

    @Test
    public void testFindAllByUserIdAndProjectId() {
        Assert.assertEquals(1,
                taskService.findAllByUserIdAndProjectId(user.getId(), project.getId())
                .size());
    }

    @Test
    public void testFindByUserIdAndId() {
        @Nullable final Task testTask = taskService.findOneById(user.getId(), task.getId());
        Assert.assertEquals(testTask.getId(), task.getId());
        Assert.assertEquals(testTask.getName(), task.getName());
        Assert.assertEquals(testTask.getDescription(), task.getDescription());
    }

    @Test
    public void testFindByUserIdAndIndex() {
        @Nullable final Task testTask = taskService.findOneByIndex(user.getId(), 0);
        Assert.assertEquals(testTask.getId(), task.getId());
        Assert.assertEquals(testTask.getName(), task.getName());
        Assert.assertEquals(testTask.getDescription(), task.getDescription());
    }

    @Test
    public void testFindByUserIdAndName() {
        @Nullable final Task testTask = taskService.findOneByName(user.getId(), task.getName());
        Assert.assertEquals(testTask.getId(), task.getId());
        Assert.assertEquals(testTask.getName(), task.getName());
        Assert.assertEquals(testTask.getDescription(), task.getDescription());
    }

    @Test
    public void testFindById() {
        @Nullable final Task testTask = taskService.findById(task.getId());
        Assert.assertEquals(testTask.getId(), task.getId());
        Assert.assertEquals(testTask.getName(), task.getName());
        Assert.assertEquals(testTask.getDescription(), task.getDescription());
    }

    @Test
    public void testRemoveAll() {
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.removeAll();
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void testRemoveAllByUserIdAndProjectId() {
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.removeAllByUserIdAndProjectId(user.getId(), project.getId());
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void testRemoveAllByUserId() {
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.removeAll(user.getId());
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void testRemoveOneByUserIdAndIndex() {
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.removeOneByIndex(user.getId(), 0);
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void testRemoveOneByUserIdAndName() {
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.removeOneByName(user.getId(), task.getName());
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void testRemoveOneById() {
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.removeOneById(task.getId());
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void testRemoveOneByUserIdAndId() {
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.removeOneById(user.getId(), task.getId());
        Assert.assertEquals(0, taskService.findAll().size());
    }

}