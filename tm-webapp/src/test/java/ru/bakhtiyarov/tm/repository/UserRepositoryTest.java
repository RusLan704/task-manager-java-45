package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.Application;
import ru.bakhtiyarov.tm.entity.User;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class UserRepositoryTest {


    @Autowired
    private IUserRepository userRepository;

    @NotNull
    private User user;

    @Before
    @Transactional
    public void initData() {
        user = new User("login", "pass");
        userRepository.save(user);
    }

    @After
    @Transactional
    public void deleteData() {
        userRepository.deleteAll();
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User user1 = userRepository.findByLogin(user.getLogin());
        Assert.assertNotNull(user1);
        Assert.assertEquals(user1.getId(), user1.getId());
        Assert.assertEquals(user1.getLogin(), user1.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user1.getPasswordHash());
    }

    @Test
    @Transactional
    public void testDeleteByLogin() {
        Assert.assertEquals(1, userRepository.findAll().size());
        userRepository.deleteByLogin(user.getLogin());
        Assert.assertEquals(0, userRepository.findAll().size());
        @Nullable final User user1 = userRepository.findById(user.getId()).orElse(null);
        Assert.assertNull(user1);
    }

}